import argparse
import yaml
from typing import List, Mapping
from pathlib import Path
import logging
from shelleybench import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("shelleybench-build-benchmark")


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--examples-list",
        type=Path,
        default=Path("examples-list.yml"),
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "--examples-path", type=Path, help="Examples base path", default=Path("examples")
    )
    parser.add_argument(
        "--logs-path", type=Path, help="Logs base path", default=Path("logs")
    )
    parser.add_argument(
        "--benchmarks-path", type=Path, help="Logs base path", default=Path("benchmarks")
    )
    parser.add_argument(
        "--runs", type=int, help="Name of the benchmark", required=True
    )
    parser.add_argument(
        "--benchmarktk-path", type=Path, default=None, help="Benchmark template path"
    )
    parser.add_argument(
        "--skip-deps", help="do not count time for dependencies", action="store_true"
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "--validity-checks", type=str, default="default", help="Select a specific validity-check method."
    )

    return parser


def create_benchmarktk(benchmark_path: Path, example_name: str, benchmark_runs: int, src_names: List[str],
                       examples_path: Path, logs_path: Path, benchmarktk_path: Path, skip_deps: bool,
                       validity_checks: str = None) -> None:
    """

    :param benchmark_path:
    :param example_name:
    :param benchmark_runs:
    :param examples_path:
    :param logs_path:
    :param src_names:
    :return:
    """

    if benchmarktk_path is not None:
        with benchmarktk_path.open(mode="r") as fd:
            benchmark = fd.read()
    else:
        benchmark = """# In this benchmark we only have one dimension we are ranging over, which is
parameters: [ example ]
# We are not comparing our program against anything, so we only need to declare
# One version being run
versions: [ origin ]
# We declare how many benchmarks we are declaring, which is only one
benchmarks: [ $$BNAME$$ ]
# Here are the values admissible by our benchmark
default_example: [
    $$BVALUES$$]
# the location of where the stats-tk binaries are (comment to lookup on PATH)
#stats-tk: stats-tk
# How many laps each series will have
run_count: $$BRUNS$$

# benchmark: the name of the benchmark
# example: the number of valves used (an integer)
# run: the run number (an integer)
# version: either the contents of 'version1' or the contents of 'version2'
log_format: "$$LOGS_PATH$$/{benchmark}/timings-{example}-{run}.json"
version_data_format: "$$LOGS_PATH$$/{benchmark}/timings-data-{example}.json"
version_stats_format: "$$LOGS_PATH$$/{benchmark}/timings-stats-{example}.json"

# In commands, braces must be escaped. Use double braces instead.
origin:
  run_cmd: &run >
    $$CLEAN$$ $$SKIP_DEPS$$ && $TIME -f "{{\\"time\\":%e, \\"mem\\":%M}}" -o {log}  make -C $$EXAMPLES_PATH$$/{benchmark} {example}.scy $$VALIDITY_CHECKS$$

  parse_cmd: &parse >
    shelleybench-parse-time "{log}"

# Use this when you do not want to actually run program
dry_run: false
verbose: true
resume: true
"""
    benchmark = benchmark.replace("$$BNAME$$", example_name)

    if skip_deps is True:
        benchmark = benchmark.replace("$$SKIP_DEPS$$", "&& make -C $$EXAMPLES_PATH$$/{benchmark} deps_{example}")
        benchmark = benchmark.replace("$$CLEAN$$", "rm -f $$EXAMPLES_PATH$$/{benchmark}/{example}.scy")

    else:
        benchmark = benchmark.replace("$$SKIP_DEPS$$", "")
        benchmark = benchmark.replace("$$CLEAN$$", "make -C $$EXAMPLES_PATH$$/{benchmark} clean")


    if validity_checks == "fsm-based":
        benchmark = benchmark.replace("$$VALIDITY_CHECKS$$", f"VALIDITY_CHECKS=--skip-mc")
    elif validity_checks == "spec-based":
        benchmark = benchmark.replace("$$VALIDITY_CHECKS$$", f"VALIDITY_CHECKS=--skip-direct")
    else:
        benchmark = benchmark.replace("$$VALIDITY_CHECKS$$", "")

    values = ""
    for src_name in src_names:
        values += f"{src_name},\n"
    benchmark = benchmark.replace("$$BVALUES$$", values)
    benchmark = benchmark.replace("$$BRUNS$$", f"{benchmark_runs}")
    benchmark = benchmark.replace("$$EXAMPLES_PATH$$", f"{examples_path}")
    benchmark = benchmark.replace("$$LOGS_PATH$$", f"{logs_path}")

    with Path(benchmark_path, "benchmarktk.yml").open("w") as f:
        f.write(benchmark)


def create_example_makefile(benchmark_path: Path, example_name: str, src_names: List[str], examples_path: Path,
                            logs_path: Path) -> None:
    template = """RUN_BENCHMARK = benchmarktk-run-benchmark
GENERATE_STATS = benchmarktk-generate-stats
COLLECT_DATA = benchmarktk-collect-data
LOGS = $$LOGS_PATH$$
EXAMPLES_PATH = $$EXAMPLES_PATH$$
EXAMPLE_NAME = $$EXAMPLE_NAME$$

all: benchmark-run

# Run benchmark

benchmark-clean:
	rm -f $(LOGS)/$(EXAMPLE_NAME)/*.json

benchmark-run:
	mkdir -p $(LOGS)/$(EXAMPLE_NAME)
	$(RUN_BENCHMARK) benchmarktk.yml

# Collect stats for benchmark
# Requires a previous run
benchmark-collect:
	$(COLLECT_DATA) benchmarktk.yml
	$(GENERATE_STATS) benchmarktk.yml

fsm-stats:
	$$FSM_STATS$$

"""

    template = template.replace("$$EXAMPLE_NAME$$", example_name)

    fsm_stats = ""
    bvalues = ""
    for src_name in src_names:
        fsm_stats += f"$(MAKE) -C $(EXAMPLES_PATH)/$(EXAMPLE_NAME) {src_name}-stats.json\n\t"
        bvalues += f"{src_name} "
    template = template.replace("$$FSM_STATS$$", fsm_stats.strip())
    template = template.replace("$$BVALUES$$", bvalues)
    template = template.replace("$$EXAMPLES_PATH$$", str(examples_path))
    template = template.replace("$$LOGS_PATH$$", str(logs_path))

    with Path(benchmark_path, "Makefile").open("w") as f:
        f.write(template)


def create_makefile(examples: Mapping[str, List[str]], output_path: Path):
    template = """all: run collect fsm-stats

run: benchmark-run

collect: benchmark-collect

fsm-stats:
	$$FSM_STATS$$

benchmark-run:
	$$BENCHMARK_RUN$$

benchmark-collect:
	$$BENCHMARK_COLLECT$$

benchmark-clean:
	$$BENCHMARK_CLEAN$$

"""
    create_benchmarktk = ""
    fsm_stats = ""
    benchmark_run = ""
    benchmark_collect = ""
    benchmark_clean = ""
    for example in examples:
        create_benchmarktk += f"make -C {example} create-benchmarktk\n	"
        if not example.startswith("bad_"):
            fsm_stats += f"make -C {example} fsm-stats\n	"
        benchmark_run += f"make -C {example} benchmark-run\n	"
        benchmark_collect += f"make -C {example} benchmark-collect\n	"
        benchmark_clean += f"make -C {example} benchmark-clean\n	"

    template = template.replace("$$CREATE_BENCHMARKTK$$", create_benchmarktk)
    template = template.replace("$$FSM_STATS$$", fsm_stats)
    template = template.replace("$$BENCHMARK_RUN$$", benchmark_run)
    template = template.replace("$$BENCHMARK_COLLECT$$", benchmark_collect)
    template = template.replace("$$BENCHMARK_CLEAN$$", benchmark_clean)

    with Path(output_path).open(mode="w") as f:
        f.write(template)


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    with args.examples_list.open(mode="r") as f:
        examples: Mapping[str, List[str]] = yaml.safe_load(f)

    args.benchmarks_path.mkdir(exist_ok=True)
    create_makefile(examples, Path(args.benchmarks_path, "Makefile"))
    for example_name in examples:
        benchmark_path = Path(args.benchmarks_path, example_name)
        benchmark_path.mkdir(parents=True, exist_ok=True)  # create example subfolder
        src_names: List[str] = [src["name"] for src in examples[example_name]]
        create_example_makefile(benchmark_path, example_name, src_names, args.examples_path.absolute(),
                                args.logs_path.absolute())
        create_benchmarktk(benchmark_path, example_name, args.runs, src_names, args.examples_path.absolute(),
                           args.logs_path.absolute(), args.benchmarktk_path, args.skip_deps, args.validity_checks)


if __name__ == "__main__":
    main()
