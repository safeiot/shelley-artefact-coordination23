import argparse
import yaml
import logging
from shelleybench import settings
from pathlib import Path
import json
import pandas as pd
import math
from scipy import stats
from typing import List

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("generate_stats")

CONFIDENCE_COEFICIENT = .95


def critical_value(sample_size: int, confidence_coeficient: float, large_sample: int = 30):
    """
    Computes the critical value given a NumPy array of samples
    and a confidence coeficient (defaults to 0.95).
    """
    alpha = 1.0 - confidence_coeficient
    if sample_size >= large_sample:
        # Z_{1 - alpha/2}:
        return stats.norm.ppf(1 - alpha / 2)
    # degrees of freedom
    df = sample_size - 1
    # t_{1 - alpha/2}:
    return stats.t.ppf(1 - alpha / 2, df)


def margin_error(sample_size: int, sample_std, confidence_coeficient: float):
    cval = critical_value(sample_size, confidence_coeficient)
    return cval * sample_std / math.sqrt(sample_size)


def get_data(path: Path):
    with path.open() as fd:
        data = json.load(fd)
        data["name"] = path
    data = pd.DataFrame.from_records([data])
    return data


def collect_data(filename, runs: int, logs_path: Path):
    filename_data = pd.DataFrame()

    for i in range(1, runs + 1):
        path = Path(logs_path, f"timings-{filename}-{i}.json")
        # print(path)
        line = get_data(path)
        # print(line)
        filename_data = pd.concat([filename_data, line])

    return filename_data


def collect_all(filenames: List[str], runs: int, logs_path: Path):
    data = pd.DataFrame()
    for filename in filenames:
        filename_data = collect_data(filename, runs, logs_path)

        # print(filename_data)
        size = filename_data.count(numeric_only=True)
        mean = filename_data.mean(numeric_only=True)
        min = filename_data.min(numeric_only=True)
        max = filename_data.max(numeric_only=True)
        std = filename_data.std(numeric_only=True)

        merr_time = margin_error(size["time"], std["time"], CONFIDENCE_COEFICIENT)
        merr_mem = margin_error(size["mem"], std["mem"], CONFIDENCE_COEFICIENT)

        # stats_row = pd.DataFrame.from_records([dict(size=filename_data.count(),
        #                                             mean=filename_data.mean(),
        #                                             min=filename_data.min(),
        #                                             max=filename_data.max(),
        #                                             std=filename_data.std()
        #                                             )])

        # print(stats_row)

        # filename_data = pd.concat([filename_data, stats_row], axis=1)

        # print(filename)
        # print(filename_data)
        # print(
        #     f'Time stats: size {size["time"]} | mean {mean["time"]} | min {min["time"]} | max {max["time"]} | std {std["time"]} | merr {merr_time}')
        # print(
        #     f'RAM stats: size {size["mem"]} | mean {mean["mem"]} | min {min["mem"]} | max {max["mem"]} | std {std["mem"]} | merr {merr_mem}')

        filename_stats = pd.DataFrame()

        filename_stats = pd.DataFrame.from_records([dict(src_name=filename,
                                                         runs=filename_data["time"].count(),
                                                         time_mean=filename_data["time"].mean(),
                                                         time_min=filename_data["time"].min(),
                                                         time_max=filename_data["time"].max(),
                                                         time_std=filename_data["time"].std(),
                                                         time_merr=merr_time,
                                                         # mem_size=filename_data["mem"].count(),
                                                         mem_mean=filename_data["mem"].mean(),
                                                         mem_min=filename_data["mem"].min(),
                                                         mem_max=filename_data["mem"].max(),
                                                         mem_std=filename_data["mem"].std(),
                                                         mem_merr=merr_mem
                                                         )])

        # print(f"Stats:\n{filename_stats}")

        data = pd.concat([data, filename_stats], ignore_index=True)

    return data

def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--examples-list",
        type=Path,
        default=Path("examples-list.yml"),
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "--logs-path", type=Path, help="Logs base path", default=Path("logs")
    )
    parser.add_argument(
        "--runs", type=int, help="Name of the benchmark", required=True
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )

    return parser

def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    with args.examples_list.open(mode="r") as f:
        examples = yaml.safe_load(f)

    for example in examples:
        filenames = [val["name"] for val in examples[example]]

        data = collect_all(filenames, args.runs, Path(args.logs_path, example))
        logger.debug(data)

        with Path(args.output, example, "stats-all.json").open(mode="w") as fd:
            data.to_json(fd, orient="records", indent=2)

    # data: pd.DataFrame = _merge_stats(examples, args.examples_path, args.logs_path)
    # data = data.rename(columns={"mean": "time_mean", "margin_error": "time_margin_error"})
    # data = data[filter_columns]
    # if args.orderby:
    #     data = data.sort_values(by=[args.orderby], ascending=True, ignore_index=True)
    #
    # logger.debug(data)

    # final = timings_data.merge(fsm_stats_data, on=list(timings_data.columns & fsm_stats_data.columns), how='outer')
    # print(final[filter_columns])
    # print(concat[["example_name", "src_name"] + fsm_filter_params + time_filter_params])


if __name__ == "__main__":
    main()
