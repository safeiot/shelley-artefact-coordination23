from pathlib import Path
import argparse
from shelleybench.save_to_wiki import parser


def save2wiki(markdown_input: Path, wiki_path_template: Path, machineid: str, benchmark_name: str, output_path: Path):
    with markdown_input.open(mode="r") as fd:
        data = fd.read()

    with wiki_path_template.open(mode="r") as fd:
        template = fd.read()

    template = template.replace("$$TABLE$$", data)
    template = template.replace("$$MACHINEID$$", machineid)
    template = template.replace("$$BENCHMARK_NAME$$", benchmark_name)

    with output_path.open(mode="w") as fd:
        fd.write(template)


def main():
    args: argparse.Namespace = parser.create_parser().parse_args()

    save2wiki(args.input, args.template, args.machineid, args.benchmark_name, args.output)


if __name__ == "__main__":
    main()
