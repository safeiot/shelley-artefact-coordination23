from pathlib import Path
import argparse


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--input",
        type=Path,
        help="Input markdown data",
        required=True
    )
    parser.add_argument(
        "--template",
        type=Path,
        help="Wiki template",
        required=True,
    )
    parser.add_argument(
        "--machineid",
        type=str,
        help="Machine ID where benchmark is being run",
        required=True
    )
    parser.add_argument(
        "--benchmark_name",
        type=str,
        help="Benchmark name",
        default="",
        required=True
    )
    parser.add_argument(
        "-o",
        "--output",
        type=Path,
        help="Wiki markdown output path",
        required=True,
    )
    return parser
