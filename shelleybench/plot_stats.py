import yaml
from typing import List, Dict
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import argparse
from dataclasses import dataclass
import importlib.util
import logging
from shelleybench import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("plot_stats")

def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "--settings",
        type=Path,
        help="plot settings",
        required=True
    )
    parser.add_argument(
        "--formats",
        "-f",
        type=str,
        nargs="+",
        help="Specify the output format",
        required=True
    ),
    parser.add_argument(
        "--name",
        type=str,
        help="Output file name",
        required=True,
    ),
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser

@dataclass
class Settings:
    ylabel: str
    xlabel: str
    title: str
    params: Dict[str, Dict]
    params_line: Dict[str, Dict]
    labels: List[str]
    params_line_yscale: str = "linear"
    bars_ylim: List[int] = None


def _autolabel(ax, rects):
    """Attach a text label above each bar in *rects*, displaying its height."""

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.2f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 15),  # 15 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


# https://matplotlib.org/3.1.1/gallery/ticks_and_spines/multiple_yaxis_with_spines.html
# https://www.delftstack.com/howto/matplotlib/how-to-hide-axis-text-ticks-and-or-tick-labels-in-matplotlib/
def create_plot(sett: Settings):
    # set width of bar
    bar_width = 0.30

    fig, ax = plt.subplots(figsize=(9, 6))

    ax.grid(True, axis="y")
    ax.set_axisbelow(True)

    idx = 0
    xticks = np.arange(len(sett.labels))
    bar_positions = []
    bar_patterns = ["", "x", "-", "/", "|", ".", "*", "\\", "o", "O"]
    for param in sett.params:
        if idx == 0:
            pos = xticks
            color = 'k'
        else:
            pos = [x + bar_width * idx for x in xticks]
            color = 'w'
        bar_positions.append(pos)
        plt.bar(pos, sett.params[param]['data'], color=color, hatch=bar_patterns[idx],
                width=bar_width,  label=param, alpha=0.5, edgecolor='k',)
        idx += 1

    first = True
    idx = 0
    default_lines = ["ko-", "kx-", "kv-", "k^-"]
    for param in sett.params_line:
        par = ax.twinx()
        if idx <= len(default_lines):
            format_color = default_lines[idx]
        else:  # more than 4 lines, use the red dashes for all
            format_color = "k--"
        idx += 1

        par1_plot, = par.plot(bar_positions[sett.params_line[param]["pos"]], sett.params_line[param]['data'],
                              format_color,label=param, alpha=0.5)

        try:
            ylim = sett.params_line[param]['ylim']
            par.set_ylim(0, ylim)
        except KeyError:
            pass

        par.set_yscale(sett.params_line_yscale)

        if first:
            par.set_ylabel(sett.params_line[param]["ylabel"])
            par.yaxis.label.set_color(par1_plot.get_color())
            tkw = dict(size=4, width=1.5)
            par.tick_params(axis='y', **tkw)
            par.tick_params(axis='y', colors=par1_plot.get_color(), **tkw)
            first = False
        else:
            par.yaxis.set_visible(False)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel(sett.ylabel)
    ax.set_xlabel(sett.xlabel)
    ax.set_title(sett.title)

    if len(sett.params) == 1:
        ax.set_xticks([p for p in xticks])
    else:
        if len(sett.params) % 2 == 0:
            ax.set_xticks([p + bar_width/2 for p in xticks])
        else:
            ax.set_xticks([p + bar_width for p in xticks])

    ax.set_xticklabels(sett.labels)

    if sett.bars_ylim:
        ax.set_ylim(sett.bars_ylim)

    if len(sett.params) > 1:
        ax.legend(loc='upper left')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # _autolabel(ax, rects1)
    # _autolabel(ax, rects2)
    # _autolabel(ax, rects3)

    fig.tight_layout()


def main():
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    data = pd.read_csv(args.input, sep=',')

    if args.settings.suffix == ".py":

        spec = importlib.util.spec_from_file_location("spec.py", args.settings)
        module_spec = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module_spec)
        spec_dict = module_spec.get_settings(data)

    else:
        with args.settings.open() as fd:
            spec_dict = yaml.safe_load(fd)

    logger.debug(spec_dict)

    sett = Settings(ylabel=spec_dict["ylabel"],
                    xlabel=spec_dict["xlabel"],
                    title=spec_dict["title"],
                    params=spec_dict["params"],
                    params_line=spec_dict["params_line"],
                    labels=spec_dict["labels"])

    if "params_line_yscale" in spec_dict:
        if spec_dict["params_line_yscale"] is not None:
            sett.params_line_yscale = spec_dict["params_line_yscale"]

    if "bars_ylim" in spec_dict:
        if spec_dict["bars_ylim"] is not None:
            sett.bars_ylim = spec_dict["bars_ylim"]

    create_plot(sett)

    for format in args.formats:
        output_path = Path(args.output, f"{args.name}")
        plt.savefig(str(output_path))


if __name__ == "__main__":
    main()
