from typing import List, Dict
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import argparse
from dataclasses import dataclass
import importlib.util
from shelleybench import settings

def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "--settings",
        type=Path,
        help="plot settings",
        required=True
    )
    parser.add_argument(
        "--formats",
        "-f",
        type=str,
        nargs="+",
        help="Specify the output format (defaults to csv)",
        required=True
    ),
    parser.add_argument(
        "--name",
        type=str,
        help="Output file name",
        required=True,
    ),
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


@dataclass
class Settings:
    ylabel: str
    xlabel: str
    title: str
    params: Dict[str, Dict]
    labels: List[str]


def _autolabel(ax, rects):
    """Attach a text label above each bar in *rects*, displaying its height."""

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.2f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 15),  # 15 points vertical offset
                    textcoords="offset points",
                    ha='center', va='top')


def create_plot(sett: Settings):
    labels = sett.labels

    x = np.arange(len(labels)/2, step=0.5) # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots(figsize=(7,4))

    ax.grid(True, axis="y")
    ax.set_axisbelow(True)

    for param in sett.params:
        rects = ax.bar(x, sett.params[param]['data'], yerr=sett.params[param]['data_err'], label=param, align='center',
                       ecolor='black', capsize=4, width=width)
        #_autolabel(ax, rects)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    # ax.set_ylabel(sett.ylabel)
    # ax.set_xlabel(sett.xlabel)
    ax.set_title(sett.title)
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    # ax.legend()

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    fig.tight_layout()


def main():
    args: argparse.Namespace = create_parser().parse_args()

    spec = importlib.util.spec_from_file_location("spec.py", args.settings)
    module_spec = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module_spec)

    data = pd.read_csv(args.input, sep=',')
    # data = data.sort_values(by=[sett.sortby], ascending=True, ignore_index=True)

    spec_dict = module_spec.get_settings(data)

    sett = Settings(ylabel=spec_dict["ylabel"],
                    xlabel=spec_dict["xlabel"],
                    title=spec_dict["title"],
                    params=spec_dict["params"],
                    labels=spec_dict["labels"])

    create_plot(sett)

    for format in args.formats:
        output_path = Path(args.output, f"{args.name}.{format}")
        plt.savefig(str(output_path))


if __name__ == "__main__":
    main()
