import argparse
from shelleybench.parse_time import parser
import json
from datetime import timedelta


def parse_time(t):
    if isinstance(t, float):
        return t
    h, m, s = t.split(":")
    return timedelta(hours=float(h), minutes=float(m), seconds=float(s)).total_seconds()


def main() -> None:
    args: argparse.Namespace = parser.create_parser().parse_args()

    data = json.load(args.input.open(mode="r"))
    print(json.dumps(parse_time(data["time"])))

if __name__ == "__main__":
    main()
