from pathlib import Path
import argparse


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Parse time statistics"
    )
    parser.add_argument(
        "input",
        type=Path,
        help="Json input file to be parsed",
    )

    return parser
