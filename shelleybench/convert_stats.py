from dataclasses import dataclass
import argparse, yaml, logging
from pathlib import Path
import pandas as pd
from shelleybench import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("convert")


@dataclass
class Columns:
    ExampleName: str = "example_name"
    SourceName: str = "src_name"
    SourceUrl: str = "src_url"
    NSubs: str = "n_subs"
    NOps: str = "n_ops"
    NFA: str = "nfa"
    IntNFA: str = "int_nfa_no_sink"
    IntDFA: str = "int_dfa_min_no_sink"
    TimeMean: str = "time_mean"
    TimeMarginError: str = "time_margin_error"
    MemMean: str = "mem_mean"
    MemMarginError: str = "mem_margin_error"
    LoC: str = "loc"


filter_columns = [Columns.ExampleName,
                  Columns.SourceName,
                  Columns.SourceUrl,
                  Columns.LoC,
                  Columns.NSubs,
                  Columns.NOps,
                  Columns.NFA,
                  Columns.IntNFA,
                  Columns.IntDFA,
                  Columns.TimeMean,
                  Columns.TimeMarginError,
                  Columns.MemMean,
                  Columns.MemMarginError,
                  ]

def hide_external_python_stats(data: pd.DataFrame):
    int_names = [x for x in data.loc[data['src_name'].str.startswith("integration_")]["src_name"]]
    names = [x.removeprefix("integration_") for x in int_names]

    def copy_value_from_name_to_int_name(column_name):
        for name, int_name in zip(names, int_names):
            data.loc[data.src_name == int_name, column_name] = data.loc[data.src_name == name, column_name].item()

    [copy_value_from_name_to_int_name(col_name) for col_name in ['loc_py', 'n_py_annots']]

    for name in names:
        data = data[data.src_name != name]

    return data

def add_totals_row(data: pd.DataFrame):
    # compute the totals row
    dtypes_before = data.dtypes
    data = data.append(data.sum(numeric_only=True), ignore_index=True)
    data = data.astype(dtypes_before)
    data.iloc[-1, data.columns.get_loc('src_name')] = "TOTAL"
    return data

def apply_common_settings(data: pd.DataFrame, input_settings: dict) -> pd.DataFrame:
    data = data[input_settings["filter_columns"]]

    if input_settings["order_by"]:
        if input_settings["order_by_ascending"]:
            ascending = input_settings["order_by_ascending"]
        else:
            ascending = False
        data = data.sort_values(by=input_settings["order_by"], ascending=ascending, ignore_index=True)

    data = add_totals_row(data)

    if input_settings["rename_columns"]:
        data = data.rename(columns=input_settings["rename_columns"])

    data.index += 1  # start index at 1

    return data


def export2latex(data: pd.DataFrame, input_settings: dict, output: Path):
    data = apply_common_settings(data, input_settings)
    logger.debug(f"data:\n{data}")
    data.to_latex(output, index=False)


def export2csv(data: pd.DataFrame, input_settings: dict, output: Path, float_format=None):
    data = apply_common_settings(data, input_settings)
    logger.debug(f"data:\n{data}")
    data.to_csv(output, index=False, float_format=float_format)


def export2wiki(data: pd.DataFrame, input_settings: dict, output: Path):
    data[Columns.SourceName] = data.apply(lambda row: f'[{row[Columns.SourceName]}]({row[Columns.SourceUrl]})', axis=1)
    data = data[input_settings["filter_columns"]]

    data = apply_common_settings(data, input_settings)
    logger.debug(f"data:\n{data}")

    buf = data.to_markdown()

    with output.open(mode="w") as f:
        f.write(buf)


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        required=True,
        help="List of examples to benchmark",
    ),
    parser.add_argument(
        "--format",
        "-f",
        required=True,
        help="Specify the output format",
    )
    parser.add_argument(
        "--settings",
        type=Path,
        required=True,
        help="Latex settings yaml file",
    ),
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    with args.settings.open() as fd:
        input_settings = yaml.safe_load(fd)

    data = pd.read_csv(args.input)

    if "fillna" in input_settings:
        data = data.fillna(input_settings['fillna'])

    if "col_datatypes" in input_settings:
        col_datatypes = input_settings['col_datatypes']
        for key, value in col_datatypes.items():
            data = data.astype({key: value})

    if "col_units" in input_settings:
        col_units = input_settings['col_units']
        for key, value in col_units.items():
            data[key] = data.apply(lambda row: row[key] / value, axis=1)

    data = hide_external_python_stats(data)

    float_format = None
    if "float_format" in input_settings:
        float_format = input_settings['float_format']
        pd.options.display.float_format = f"{float_format}".format
        logger.debug(f"float format: {float_format}")

    logger.debug(f"filter columns: {input_settings['filter_columns']}")
    logger.debug(f"rename columns: {input_settings['rename_columns']}")
    logger.debug(f"order by: {input_settings['order_by']}")

    if args.format == 'latex':
        export2latex(data, input_settings, args.output)
    elif args.format == 'markdown':
        export2wiki(data, input_settings, args.output)
    elif args.format == 'csv':
        export2csv(data, input_settings, args.output)


if __name__ == "__main__":
    main()