import os
import argparse
import yaml
import logging
from typing import List
from pathlib import Path
from shelleybench import settings
from natsort import natsorted, ns

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("list_examples")


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate yaml with list of examples"
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "--examples-path",
        type=Path,
        help="Examples base path",
        required=True,
    )
    parser.add_argument(
        "--base-url",
        type=str,
        help="Base url",
        default=""
    )
    parser.add_argument(
        "--target-ext",
        type=str,
        help="Target files extension (yml or shy)",
        default=".shy"
    )
    parser.add_argument(
        "--exclude", type=str, nargs="*", default=[], help="List of exclude files"
    )
    parser.add_argument(
        "--exclude-files",
        type=Path,
        help="YAML file with filenames that should be excluded",
        default=None
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        default=Path("examples-list.yml"),
        help="Path to the generated benchmarktk file",
        required=False,
    )

    return parser


def list_examples(examples_path: Path, exclude: List[str], exclude_files: List[str], base_url: str, output_path: Path,
                  target_ext: str = ".shy"):
    examples_dict = dict()

    logger.debug(exclude)
    logger.debug(exclude_files)

    for foldername in os.listdir(examples_path):
        example_path = Path(examples_path, foldername).absolute()
        if not os.path.isfile(example_path) and not foldername in exclude:
            scy_files: List[str] = list()
            list_example_dir: List[str] = natsorted(os.listdir(example_path), key=lambda y: y.lower())
            logger.debug(list_example_dir)
            for filename in list_example_dir:
                if filename.endswith(target_ext) and not filename.startswith("uses") and not filename in exclude_files:
                    logger.debug(filename)
                    name, ext = os.path.splitext(filename)
                    url = f"{base_url}/{foldername}/{filename}"
                    file_info = dict(name=name, url=url)
                    scy_files.append(file_info)
            # logger.debug(scy_files)
            examples_dict[foldername] = scy_files

    with output_path.open(mode="w") as f:
        yaml.dump(examples_dict, f, sort_keys=False)


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    exclude_files = []
    if args.exclude_files:
        with args.exclude_files.open("r") as f:
            exclude_files = yaml.safe_load(f)

    list_examples(args.examples_path, args.exclude, exclude_files, args.base_url, args.output, args.target_ext)


if __name__ == "__main__":
    main()
