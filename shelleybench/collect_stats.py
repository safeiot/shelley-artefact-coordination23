from dataclasses import dataclass
import argparse
import yaml
import logging
from typing import List, Mapping, Tuple
from pathlib import Path
import pandas as pd
from shelleybench import settings
import shlex, subprocess

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("collect")


@dataclass
class Columns:
    ExampleName: str = "example_name"
    SourceName: str = "src_name"
    SourceUrl: str = "src_url"
    NSubs: str = "n_subs"
    NOps: str = "n_ops"
    NCalls: str = "n_calls"
    NFA: str = "nfa"
    IntNFA: str = "int_nfa_no_sink"
    IntDFA: str = "int_dfa_min_no_sink"
    TimeMean: str = "time_mean"
    TimeMarginError: str = "time_margin_error"
    MemMean: str = "mem_mean"
    MemMarginError: str = "mem_margin_error"
    LoC: str = "loc"
    LoCpy: str = "loc_py"
    LoCSMVSystem: str = "loc_smv_system"
    LoCSMVIntegration: str = "loc_smv_int"
    NClaims: str = "n_claims"
    NPyAnnots: str = "n_py_annots"


filter_columns = [Columns.ExampleName,
                  Columns.SourceName,
                  Columns.SourceUrl,
                  Columns.LoCpy,
                  Columns.LoC,
                  Columns.LoCSMVSystem,
                  Columns.LoCSMVIntegration,
                  Columns.NSubs,
                  Columns.NOps,
                  Columns.NCalls,
                  Columns.NClaims,
                  Columns.NPyAnnots,
                  Columns.NFA,
                  Columns.IntNFA,
                  Columns.IntDFA,
                  Columns.TimeMean,
                  Columns.TimeMarginError,
                  Columns.MemMean,
                  Columns.MemMarginError,
                  ]


def _merge_stats(examples: Mapping[str, List[str]], examples_path: Path, logs_path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    for example_name in examples:  # folder of example
        sources: List[dict] = examples[example_name]
        time_mem_stats = _collect_stats(Path(logs_path, f"{example_name}", f"stats-all.json"))
        example_data = pd.DataFrame()
        for src in sources:
            fsm_stats_row = _collect_stats(Path(examples_path, f"{example_name}", f"{src['name']}-stats.json"))

            loc_py_row = _collect_count_loc_py(
                Path(examples_path, example_name, f"{src['name']}.py"))

            loc_py_annots = _collect_count_loc_py_annots(
                Path(examples_path, example_name, f"{src['name']}.py"))

            loc_shy_row = _collect_count_loc(
                Path(examples_path, example_name, f"{src['name']}.shy"))

            loc_smv_system_row = _collect_count_loc_smv(
                Path(examples_path, example_name, f"{src['name']}.smv"), Columns.LoCSMVSystem)

            loc_smv_integration_row = _collect_count_loc_smv(
                Path(examples_path, example_name, f"{src['name']}-i.smv"), Columns.LoCSMVIntegration)

            loc_claims = _collect_count_claims(
                Path(examples_path, example_name, f"{src['name']}.shy"))

            subsystems_stats_row = _collect_count_subsystems(
                Path(examples_path, example_name, f"{src['name']}.shy"))

            labels_row = pd.DataFrame.from_records(
                [dict(example_name=example_name, src_name=src['name'], src_url=src['url'])])

            merge_rows = pd.concat(
                [labels_row, subsystems_stats_row, fsm_stats_row, loc_py_row, loc_shy_row, loc_smv_system_row, loc_smv_integration_row, loc_claims, loc_py_annots],
                axis=1)

            example_data = pd.concat([example_data, merge_rows], ignore_index=True)
        logger.debug(f"example data\n: {example_data}")
        logger.debug(f"time_mem_stats data\n: {time_mem_stats}")
        example_data = example_data.merge(time_mem_stats)
        data = pd.concat([data, example_data])

    return data


def _collect_stats(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    if path.exists():
        # load json with fsm stats
        row = pd.read_json(path, orient='record')
    else:
        logger.warning(f"Skip file without stats: {path}")
        row = pd.Series()  # empty row

    data = pd.concat([data, row], ignore_index=True)
    return data


def _collect_count_subsystems(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    from shelley.parsers.shelley_lark_parser import parser as lark_parser, ShelleyLanguage
    from shelley.ast.devices import Device
    from shelley.ast.visitors.count_calls import CountCalls

    with path.open() as f:
        shelley_device: Device = ShelleyLanguage().transform(lark_parser.parse(f.read()))

    visitor = CountCalls()
    shelley_device.triggers.accept(visitor)
    n_calls : int = visitor.count

    row = pd.DataFrame.from_records([dict(n_subs=len(shelley_device.components), n_ops=len(shelley_device.events), n_calls=n_calls)])
    data = pd.concat([data, row], ignore_index=True)

    return data

def _collect_count_loc_py(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    # process = subprocess.run(['wc', '-l', path], stdout=subprocess.PIPE, universal_newlines=True)
    # loc = int(process.stdout.strip().split(' ')[0])

    if path.exists():
        # https://stackoverflow.com/questions/114814/count-non-blank-lines-of-code-in-bash
        # https://unix.stackexchange.com/questions/11305/show-all-the-file-up-to-the-match
        # exclude comments, blank lines and stop on 'test_system'
        cmd = f"cat {path} | $SED '/^\s*#/d;' | wc -l"
        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        loc = int(output.strip())
    else:
        loc = 0

    row = pd.DataFrame.from_records([{Columns.LoCpy: loc}])
    data = pd.concat([data, row], ignore_index=True)

    return data

def _collect_count_loc_py_annots(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    # process = subprocess.run(['wc', '-l', path], stdout=subprocess.PIPE, universal_newlines=True)
    # loc = int(process.stdout.strip().split(' ')[0])

    if path.exists():
        # https://stackoverflow.com/questions/114814/count-non-blank-lines-of-code-in-bash
        # https://unix.stackexchange.com/questions/11305/show-all-the-file-up-to-the-match
        # exclude comments, blank lines and stop on 'test_system'
        cmd = f"cat {path} | $SED '/^\s*#/d;' | grep '@' | wc -l"
        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        loc = int(output.strip())
    else:
        loc = None

    row = pd.DataFrame.from_records([{Columns.NPyAnnots: loc}])
    data = pd.concat([data, row], ignore_index=True)

    return data

def _collect_count_loc(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    # process = subprocess.run(['wc', '-l', path], stdout=subprocess.PIPE, universal_newlines=True)
    # loc = int(process.stdout.strip().split(' ')[0])

    # https://stackoverflow.com/questions/114814/count-non-blank-lines-of-code-in-bash
    # https://unix.stackexchange.com/questions/11305/show-all-the-file-up-to-the-match
    # exclude comments, blank lines and stop on 'test_system'
    cmd = f"cat {path} | $SED '/^\s*#/d;/^\s*$/d;/.*test_system.*/{{s///;q;}}' | wc -l"
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    loc = int(output.strip())

    row = pd.DataFrame.from_records([{Columns.LoC: loc}])
    data = pd.concat([data, row], ignore_index=True)

    return data


def _collect_count_loc_smv(path: Path, column_name: str) -> pd.DataFrame:
    data = pd.DataFrame()

    # process = subprocess.run(['wc', '-l', path], stdout=subprocess.PIPE, universal_newlines=True)
    # loc = int(process.stdout.strip().split(' ')[0])

    if path.exists():
        # https://stackoverflow.com/questions/114814/count-non-blank-lines-of-code-in-bash
        # https://unix.stackexchange.com/questions/11305/show-all-the-file-up-to-the-match
        # exclude comments, blank lines and stop on 'test_system'
        cmd = f"cat {path} | $SED '/^\s*#/d;/^\s*--/d;/^\s*$/d;' | wc -l"
        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        loc = int(output.strip())
    else:
        loc = 0

    row = pd.DataFrame.from_records([{column_name: loc}])
    data = pd.concat([data, row], ignore_index=True)

    return data

def _collect_count_claims(path: Path) -> pd.DataFrame:
    data = pd.DataFrame()

    # process = subprocess.run(['wc', '-l', path], stdout=subprocess.PIPE, universal_newlines=True)
    # loc = int(process.stdout.strip().split(' ')[0])

    if path.exists():
        # https://stackoverflow.com/questions/114814/count-non-blank-lines-of-code-in-bash
        # https://unix.stackexchange.com/questions/11305/show-all-the-file-up-to-the-match
        # exclude comments, blank lines and stop on 'test_system'
        cmd = f"cat {path} | $SED '/^\s*#/d;' | grep 'integration check' | wc -l"
        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        loc = int(output.strip())

        cmd = f"cat {path} | $SED '/^\s*#/d;' | grep 'system check' | wc -l"
        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        loc += int(output.strip())
    else:
        loc = 0

    row = pd.DataFrame.from_records([{Columns.NClaims: loc}])
    data = pd.concat([data, row], ignore_index=True)

    return data


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "--examples-list",
        type=Path,
        default=Path("examples-list.yml"),
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "--examples-path", type=Path, help="Examples base path", required=True
    )
    parser.add_argument(
        "--logs-path", type=Path, help="Logs base path", required=True
    )
    parser.add_argument(
        "--format",
        "-f",
        default="csv",
        help="Specify the output format (defaults to csv)",
    )
    parser.add_argument(
        "--orderby",
        default=None,
        help="Specify the column name to order values",
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    settings.VERBOSE = args.verbosity
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)

    with args.examples_list.open(mode="r") as f:
        examples = yaml.safe_load(f)

    data: pd.DataFrame = _merge_stats(examples, args.examples_path, args.logs_path)

    logger.debug(f"{args.examples_list}\n{data}")

    data = data.rename(columns={"time_merr": "time_margin_error", "mem_merr": "mem_margin_error"})

    data = data[filter_columns]
    if args.orderby:
        data = data.sort_values(by=[args.orderby], ascending=True, ignore_index=True)

    logger.debug(f"{args.examples_list}\n{data}")

    # final = timings_data.merge(fsm_stats_data, on=list(timings_data.columns & fsm_stats_data.columns), how='outer')
    # print(final[filter_columns])
    # print(concat[["example_name", "src_name"] + fsm_filter_params + time_filter_params])

    if args.format == 'csv':
        data.to_csv(args.output, index=False)
    elif args.format == 'json':
        data.to_json(args.output)
    elif args.format == 'xls':
        data.to_excel(args.output, sheet_name='stats')


if __name__ == "__main__":
    main()
