import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import pandas as pd
from pathlib import Path
import argparse


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "--log",
        action="store_true",
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def create_plot(data1, log=False):
    fig = plt.figure(figsize=(6.4,2.8))
    plt.grid(True, axis="both")
    # print(fig.get_size_inches()*fig.dpi)

    plt.plot(range(1, 82, 10), 'time_mean', data=data1, marker='o', color='C0', linewidth=2)
    # plt.ylabel('Mean Time (s)')
    # plt.xlabel('Operations')
    plt.xticks(range(11, 82, 10))
    plt.yticks(np.arange(0, 9, 2))
    if log is True:
        plt.yscale('log', base=2)

    # show legend
    # plt.legend()

    plt.tight_layout()


def main():
    args: argparse.Namespace = create_parser().parse_args()
    data1 = pd.read_csv(args.input, sep=',')
    create_plot(data1, args.log)
    plt.savefig(str(args.output))


if __name__ == "__main__":
    main()
