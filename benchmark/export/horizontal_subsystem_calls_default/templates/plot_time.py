import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import argparse


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "--log",
        action="store_true",
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def create_plot(data1, log=False):
    # multiple line plots
    plt.figure(figsize=(6.4, 2.8))
    plt.grid(True, axis="both")
    plt.plot(range(1, 321, 10), 'time_mean', data=data1, marker='o', color='C0', linewidth=2)
    plt.xticks(range(31, 312, 40))
    # plt.ylabel('Mean Time (s)')
    # plt.xlabel('Calls')
    if log is True:
        plt.yscale('log', base=2)

    # show legend
    #plt.legend()
    plt.tight_layout()


def main():
    args: argparse.Namespace = create_parser().parse_args()

    data1 = pd.read_csv(args.input, sep=',')

    data1 = data1[data1.src_name != 'valve']
    create_plot(data1, args.log)

    plt.savefig(str(args.output))


if __name__ == "__main__":
    main()
