def get_settings(data):

    data = data[data.src_name != 'TOTAL']
    data = data.sort_values(by='src_name', ascending=True, ignore_index=True)

    params = {
        "Mean": {
            "data": data.time_mean.to_list(),
            "data_err": data.time_margin_error.to_list()
        }
    }

    labels = []
    for src_name in data.src_name:
        if src_name in labels:
            src_name = f"{src_name}*"
        labels.append(src_name)


    sett = dict(ylabel="Time (s)",
                xlabel="Systems",
                title="",#Aquamote Case Study (Time)",
                params=params,
                labels=labels,
                )
    return sett
