from pathlib import Path
import argparse


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        required=True,
        help="List of examples to benchmark",
    )
    return parser


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    with Path(args.input).open() as f:
        original: str = str(f.read())

    original = original.replace("app_extended", "app")
    original = original.replace("controller_extended", "controller")
    original = original.replace("wireless_extended", "wireless")
    original = original.replace("sectors_extended", "sectors")

    original = original.replace("app\_extended", "app")
    original = original.replace("controller\_extended", "controller")
    original = original.replace("wireless\_extended", "wireless")
    original = original.replace("sectors\_extended", "sectors")

    with Path(args.input).open(mode="w") as f:
        f.write(original)


if __name__ == "__main__":
    main()
