from pathlib import Path
import argparse
import pandas as pd


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        required=True,
        help="List of examples to benchmark",
    )
    return parser


def main() -> None:
    args: argparse.Namespace = create_parser().parse_args()

    df = pd.read_csv(args.input)
    filter = ["app", "controller", "wireless", "sectors"]
    df_filtered = df[~df['src_name'].isin(filter)]
    # print(df_filtered.head(15))
    df_filtered.to_csv(args.input, index=False)


if __name__ == "__main__":
    main()
