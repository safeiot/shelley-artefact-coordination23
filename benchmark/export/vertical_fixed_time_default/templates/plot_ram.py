from typing import List, Dict
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import argparse
from dataclasses import dataclass
import importlib.util
from shelleybench import settings


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def create_plot(data):
    # multiple line plots

    plt.grid(True, axis="both")

    y_labels = [f"$12^{{{i}}}$" for i in range(1, 13)]  # create 12 points

    plt.plot(y_labels, 'mem_mean', data=data, marker='o', color='C0', linewidth=2)
    plt.ylabel('Mean RAM (Kb)')
    plt.xlabel('Total represented valves')
    plt.xticks(range(1, len(data) + 1, 2)) # filter the 12 points

    # show legend
    plt.legend()



def main():
    args: argparse.Namespace = create_parser().parse_args()

    data = pd.read_csv(args.input, sep=',')

    data = data[data.src_name != 'L0_Valve']

    create_plot(data)

    plt.savefig(str(args.output))


if __name__ == "__main__":
    main()
