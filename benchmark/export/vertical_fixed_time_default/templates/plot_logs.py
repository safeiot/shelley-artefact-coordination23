# Python program showing
# Graphical representation  
# of log() function
import numpy as np
import matplotlib.pyplot as plt

in_array = [i for i in range(1, 50)]
out_array = np.log2(in_array)
# print("out_array : ", out_array)

# plt.plot(in_array, in_array,
#          color='blue', marker="*")

# red for numpy.log()
plt.plot(in_array, out_array,
         color='red', marker="o")

plt.title("numpy.log()")
plt.xlabel("out_array")
plt.ylabel("in_array")
plt.savefig("log2.png")

plt.plot(in_array, out_array,
         color='red', marker="o")
plt.yscale('log', base=2)
plt.xscale('log', base=2)
plt.title("numpy.log()")
plt.xlabel("out_array")
plt.ylabel("in_array")
plt.savefig("log2_logscale.png")

