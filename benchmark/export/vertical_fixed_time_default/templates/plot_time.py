from typing import List, Dict
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import argparse
from dataclasses import dataclass
import importlib.util
from shelleybench import settings


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--input",
        type=Path,
        help="List of examples to benchmark",
        required=True
    )
    parser.add_argument(
        "--log",
        action="store_true",
        help="List of examples to benchmark",
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        help="Path to the generated stats file",
        required=True,
    )
    return parser


def create_plot(data, log=False):
    # multiple line plots
    plt.figure(figsize=(6.4, 2.8))
    plt.grid(True, axis="both")
    x_base = 2
    y_base = 2

    systems_per_level = 12

    x_labels = [pow(systems_per_level, i) for i in range(1, 13)] # create 12 points (not to be confused with x_base)
    print(x_labels)
    # print(data['time_mean'])
    # print(data)

    plt.plot(x_labels, data['time_mean'], marker='o', color='C0', linewidth=2)
    # plt.ylabel('Mean Time (s)')
    # plt.xlabel('Total represented subsystems')
    plt.xticks(range(1, len(data) + 1, 2)) # filter the 12 points
    plt.yscale('log', base=y_base)
    plt.xscale('log', base=x_base)
    if log is True:
        plt.yscale('log', base=2)

    # show legend
    #plt.legend()
    plt.tight_layout()


def main():
    args: argparse.Namespace = create_parser().parse_args()

    data = pd.read_csv(args.input, sep=',')

    data = data[data.src_name != 'L0_Valve']


    create_plot(data, args.log)

    plt.savefig(str(args.output))


if __name__ == "__main__":
    main()
