include ../settings.mk

all: init _export

init:
	mkdir -p $(OUTPUT_PATH)
	mkdir -p $(TEMP_PATH)

#publish: init _export
#	-cp $(OUTPUT_PATH)/*.pdf $(EXPORT_PATH)
#	-cp $(OUTPUT_PATH)/*.tex $(EXPORT_PATH)

clean:
	rm -rf $(OUTPUT_PATH)

.PHONY: clean publish update _export