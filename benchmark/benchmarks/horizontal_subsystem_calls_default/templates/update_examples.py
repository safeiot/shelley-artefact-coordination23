import argparse
from pathlib import Path
from typing import List
import logging
import yaml

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("macromicro.update_examples")

template_makefile = """SHELLEYC = shelleyc
SHELLEYV = shelleyv
SHELLEYS = shelleys
SHELLEYMC = shelleymc
USES = -u uses.yml

%.scy: %.shy
	$(SHELLEYMC) $(USES) -s $< $(VALIDITY_CHECKS) $(DEBUG)

# Generate the integration diagram
%-i.scy:  %.shy
	$(SHELLEYC) $(USES) -d $< --skip-checks --no-output -i $@ $(DEBUG)

# Generate FSM stats
%-stats.json: %.scy %-i.scy
	$(SHELLEYS) $^ -o $@ --int-nfa

all: scy

dfa-pdf: {VALVE_GROUP_FILES_DFA_PDF}

nfa-pdf: {VALVE_GROUP_FILES_NFA_PDF}

pdf: valve.pdf {VALVE_GROUP_FILES_PDF}

scy: valve.scy {VALVE_GROUP_FILES_SCY}

USES = -u uses.yml

deps: valve.scy

{VALVE_GROUP_FILES_SCY_SECTION}
clean:
	rm -f *.scy *.pdf *.gv *-stats.json *.smv

clean-all:
	rm -f *.scy *.pdf *.gv *-stats.json *.smv

.SUFFIXES: .shy .scy .smv

.PRECIOUS: %-i.scy
"""


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--micro", "-m", type=int, nargs="*", help="The list of micro operations"
    ),
    parser.add_argument(
        "--micro_step", type=int, default=1, help="The number of repetitions in micro"
    )
    parser.add_argument(
        "--macro", "-M", type=int, nargs="*", help="The list of macro operations"
    )
    parser.add_argument(
        "--macro_step", type=int, default=1, help="The number of repetitions in micro"
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        default=Path("."),
        help="Path to the generated source files",
        required=False,
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    return parser


def create_uses(path: Path):
    uses = {
        "Valve": "valve.scy",
    }

    with path.open(mode="w") as f:
        yaml.dump(uses, f, default_flow_style=None, sort_keys=False)


def create_valve(path: Path):
    template = """base Valve {
 initial final x1 -> x1;
}"""

    with path.open(mode="w") as f:
        f.write(template)


def create_valve_group(path: Path, subsystem_call_repetitions: int):
    template = """ValveGroup (v: Valve) {
 initial final go1 -> go1 {
  $$CALLS$$ 
 }
}"""

    repetition_list = "v.x1;"
    for c in range(2, subsystem_call_repetitions +1):
        repetition_list+= f" v.x1;"

    template = template.replace("$$CALLS$$", repetition_list)

    with path.open(mode="w") as f:
        f.write(template)


def create_makefile(path: Path, macro_list: List[int], micro_list: List[int]):
    scy = ''
    scy_section = ''

    for macro in macro_list:
        for micro in micro_list:
            scy += f"valve_group{macro}_{micro}.scy "
            scy_section += f"valve_group{macro}_{micro}.scy : valve_group{macro}_{micro}.shy valve.scy\n	$(SHELLEYMC) $(USES) -s $< $(VALIDITY_CHECKS) $(DEBUG)\n\n"

    result = template_makefile.replace('{VALVE_GROUP_FILES_SCY}', scy)
    result = result.replace('{VALVE_GROUP_FILES_SCY_SECTION}', scy_section)

    logger.debug(f"{scy}")

    with path.open(mode="w") as f:
        f.write(result)


if __name__ == '__main__':
    args: argparse.Namespace = create_parser().parse_args()

    if args.verbosity:
        logger.setLevel(logging.DEBUG)

    create_uses(Path(args.output, "uses.yml"))

    macro_list: List[int] = args.macro
    if len(macro_list) == 1:
        macro_list = [x for x in range(1, macro_list[0] + args.macro_step, args.macro_step)]

    micro_list: List[int] = args.micro
    if len(micro_list) == 1:
        micro_list = [x for x in range(1, micro_list[0] + args.micro_step, args.micro_step)]

    create_uses(Path(args.output, "uses.yml"))

    create_valve(Path(args.output, "valve.shy"))

    for n_calls in micro_list:
        create_valve_group(Path(args.output, f"valve_group1_{n_calls}.shy"), n_calls)

    create_makefile(Path(args.output, "Makefile"), macro_list, micro_list)
