EXAMPLES_PATH = examples_auto
LOGS_PATH = logs
TEMP_PATH = temp
OUTPUT_PATH = output
BENCHMARKS_PATH = benchmarks
TEMPLATES_PATH = templates
BENCHMARK_MACHINE = apple-m1-8-cores-16gb
GIT_BRANCH_NAME = paper-frankenstein
BASE_URL = https://gitlab.com/safeiot/shelley-benchmarking/-/blob/$(GIT_BRANCH_NAME)/benchmarks/$(BENCHMARK_NAME)/$(EXAMPLES_PATH)
#VERBOSE = -v
RUNS = 11

