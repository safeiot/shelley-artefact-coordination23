include ../settings.mk

MAKEFLAGS += --no-print-directory

all: build run collect

build: init setup-examples setup-benchmark

init:
	mkdir -p $(BENCHMARKS_PATH)
	mkdir -p $(LOGS_PATH)
	mkdir -p $(OUTPUT_PATH)

setup-examples: _setup

setup-benchmark:
	shelleybench-build-benchmark $(VERBOSE) --examples-list $(BENCHMARKS_PATH)/examples-list.yml --examples-path $(EXAMPLES_PATH) --logs-path $(LOGS_PATH) --runs $(RUNS) $(SKIP_DEPS) --validity-checks $(CHECK_TYPE)

run: benchmarktk-run

collect: fsm-stats
	shelleybench-generate-stats $(VERBOSE) --examples-list $(BENCHMARKS_PATH)/examples-list.yml --runs $(RUNS) --logs-path $(LOGS_PATH) -o $(LOGS_PATH)
	shelleybench-collect-stats $(VERBOSE) --examples-list $(BENCHMARKS_PATH)/examples-list.yml --examples-path $(EXAMPLES_PATH) --logs-path $(LOGS_PATH) --format=csv -o $(OUTPUT_PATH)/$(BENCHMARK_NAME).csv # --orderby time_mean n_subs

benchmarktk-run:
	@echo "Number of runs: $(RUNS)"
	make -C $(BENCHMARKS_PATH) run

fsm-stats:
	make -C $(BENCHMARKS_PATH) fsm-stats

#clean:
#	find examples_auto/$(BENCHMARK_NAME) -name "*-stats.json" -type f -delete 2>/dev/null
#	rm -rf $(OUTPUT_PATH)

clean-all: clean
	rm -rf $(OUTPUT_PATH)
	rm -rf examples_auto
	rm -rf $(BENCHMARKS_PATH)
	rm -rf $(LOGS_PATH)

.PHONY: clean clean-all init build run collect benchmarktk-run fsm-stats publish update _export