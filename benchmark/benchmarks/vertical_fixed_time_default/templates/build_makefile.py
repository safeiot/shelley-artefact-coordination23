import argparse
import yaml
from pathlib import Path
from typing import Dict

template = """
SHELLEYC = shelleyc
SHELLEYV = shelleyv
SHELLEYS = shelleys
SHELLEYMC = shelleymc

USES = -u uses.yml

%.scy: %.shy
	$(SHELLEYMC) $(USES) -s $< $(VALIDITY_CHECKS) $(DEBUG)

# Generate the integration diagram
%-i.scy:  %.shy
	$(SHELLEYC) $(USES) -d $< --skip-checks --no-output -i $@ $(DEBUG)

# Generate FSM stats
%-stats.json: %.scy %-i.scy
	$(SHELLEYS) $^ -o $@ --int-nfa

{VALVE_GROUP_FILES_SCY_SECTION}

{VALVE_GROUP_FILES_INT_SECTION}

clean:
	rm -f *.scy *.pdf *-stats.json *.smv

.SUFFIXES: .shy .scy *.smv

.PRECIOUS: %-i.scy
"""


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--targets",
        type=Path,
        help="Path to the makefile targets",
        required=False,
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        default=Path("."),
        help="Path to the generated source files",
        required=False,
    )

    return parser


def create_makefile(path: Path, targets: Dict[str, str]):
    scy_section = ''
    int_section = ''

    for name, dep in targets.items():  # create only files corresponding to n_components list
        scy_section += f"{name}.scy: {name}.shy {dep}.scy\n\n"
        int_section += f"{name}-i.scy: {name}.shy\n	$(SHELLEYC) $(USES) -d $< --skip-checks --no-output -i $@\n\n"

    result = template.replace('{VALVE_GROUP_FILES_SCY_SECTION}', scy_section.strip())
    result = result.replace('{VALVE_GROUP_FILES_INT_SECTION}', int_section.strip())

    with path.open(mode="w") as f:
        f.write(result)



if __name__ == '__main__':
    args: argparse.Namespace = create_parser().parse_args()

    with args.targets.open(mode="r") as f:
        targets = yaml.safe_load(f)

    create_makefile(Path(args.output, "Makefile"), targets)
