import yaml, argparse
from pathlib import Path
from typing import List, Dict


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--topsystem", type=str, nargs="*", help="The number of repetitions in integration"
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        default=Path("."),
        help="Path to the generated source files",
        required=False,
    )
    return parser


def dump(path: Path, uses: Dict[str, str]):
    with path.open(mode="w") as f:
        yaml.dump(uses, f, sort_keys=False)


def build_valve(path: Path):
    template="""base Valve {
 initial final x1 -> x1 ;
}
"""

    with path.open(mode="w") as f:
        f.write(template)


def build_valve_group(path: Path, system_name: str, subsystem_name: str, n_subsystems: int, level_idx: int):
    template = """$$SYS_NAME$$ ($$SUBSYSTEMS$$) {
 initial final go -> go {
  $$SUB_OPS$$
 }
}"""

    template = template.replace("$$SYS_NAME$$", f"{system_name}")

    subsystems = f"v{1}: {subsystem_name}"
    for i in range(2, n_subsystems + 1):
        subsystems += f", v{i}: {subsystem_name}"

    # subsystems ops
    subsystem_ops = ""
    if level_idx == 1:
        subsystem_ops += f"v{1}.x{1};"
        for n in range(2, n_subsystems + 1):
            subsystem_ops += f" v{n}.x{1};"
    else:
        for i in range(1, n_subsystems + 1):
            subsystem_ops += f"v{i}.go; "

    template = template.replace("$$SUBSYSTEMS$$", subsystems)
    template = template.replace("$$SUB_OPS$$", subsystem_ops)

    with path.open(mode="w") as f:
        f.write(template)


def get_system_name(levels: List[int]):
    name: str = f"L{len(levels)}_"
    for l in levels:
        name += f"{l}x"
    name = name[:-1]  # remove trailing x
    return name


def get_subsystem_name(levels: List[str]):
    if len(levels) > 1:
        levels = levels[1:]  # first number is the system
        name = get_system_name(levels)
    else:
        name = "Valve"
    return name


def get_basename(name: str):
    return name.split("Valves")[0]


if __name__ == '__main__':

    args: argparse.Namespace = create_parser().parse_args()

    uses: Dict[str, str] = dict()

    build_valve(Path(args.output, "L0_Valve.shy"))
    uses["Valve"] = "L0_Valve.scy"

    makefile_targets = {}
    exclude = ["uses.yml", "makefile_targets.yml", "exclude.yml"]  # this is helpful for filtering examples later

    for topsystem in args.topsystem:

        levels: List[int] = [int(n) for n in topsystem.split("x")]

        for idx in range(len(levels)):  # iterate over topsystem and its subsystems
            name: str = get_system_name(levels[idx:])
            subsystem_name: str = get_subsystem_name(levels[idx:])
            n_subsystems = levels[idx]
            level_idx = len(levels) - idx
            filename: str = f"{name}"
            uses[name] = f"{filename}.scy"
            # print(f"{filename} | {name} | {subsystem_name} | {n_subsystems} | {args.nvalveops}")
            build_valve_group(Path(args.output, f"{filename}.shy"), name, subsystem_name, n_subsystems, level_idx)

            if get_basename(name) not in args.topsystem:
                exclude.append(f"{filename}.yml")

            if (level_idx) == 1:
                makefile_targets[filename] = f"L0_Valve"
            else:
                makefile_targets[filename] = f"{subsystem_name}"

    dump(Path(args.output, "uses.yml"), uses)

    dump(Path(args.output, "makefile_targets.yml"), makefile_targets)
    dump(Path(args.output, "exclude.yml"), exclude)
