## Varying the number of macro operations


### Goal
In this benchmark we measure the impact of varying the number of macro operations.
Every macro operation has the same number of micro operations.

#### What does it change?
* Number of macro operations 

#### Description

`Valve` is a basic system, with a certain number of macro operations.

Then, several `ValveGroups` are created using the same number of `Valves` but changing the number of macro operations.

The micro operations are a repetition of the doublet `v.on,v.off`.


