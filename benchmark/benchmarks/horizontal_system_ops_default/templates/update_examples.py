import argparse
from pathlib import Path
from typing import List, Dict
import logging
import yaml

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("horizontal_operations.build_examples")

template_makefile = """SHELLEYC = shelleyc
SHELLEYMC = shelleymc
SHELLEYV = shelleyv
SHELLEYS = shelleys
USES = -u uses.yml

%.scy: %.shy
	$(SHELLEYMC) $(USES) -s $< $(VALIDITY_CHECKS) $(DEBUG)

# Generate the integration diagram
%-i.scy:  %.shy
	$(SHELLEYC) $(USES) -d $< --skip-checks --no-output -i $@ $(DEBUG)

# Generate FSM stats
%-stats.json: %.scy %-i.scy
	$(SHELLEYS) $^ -o $@ --int-nfa

all: scy

scy: {VALVE_FILES_SCY}

{VALVE_FILES_SCY_SECTION}

clean:
	rm -f *.scy *.pdf *.gv *-stats.json *.smv

.SUFFIXES: .shy .scy

.PRECIOUS: %-i.scy
"""


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Generate benchmark input files"
    )
    parser.add_argument(
        "--micro", "-m", type=int, nargs="*", help="The list of micro operations"
    ),
    parser.add_argument(
        "--micro_step", type=int, default=1, help="The number of repetitions in micro"
    )
    parser.add_argument(
        "--macro", "-M", type=int, nargs="*", help="The list of macro operations"
    )
    parser.add_argument(
        "--macro_step", type=int, default=1, help="The number of repetitions in micro"
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        type=Path,
        default=Path("."),
        help="Path to the generated source files",
        required=False,
    )
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )
    return parser


def dump(path: Path, uses: Dict[str, str]):
    with path.open(mode="w") as f:
        yaml.dump(uses, f, sort_keys=False)


def create_valve(path: Path, n_macro: int):

    template = """base Valve {
$$OPS$$}"""

    operations = ""

    for macro in range(1, n_macro + 1):
        if macro == 1:
            operations += f" initial final x{macro} -> "
        else:
            operations += f" final x{macro} -> "
        operations += f"x1"
        for x_macro in range(2, n_macro + 1):
            operations += f", x{x_macro}"
        operations += ";\n"

    template = template.replace("$$OPS$$", operations)

    with path.open(mode="w") as f:
        f.write(template)

def create_makefile(path: Path, macro_list: List[int], micro_list: List[int]):
    scy_valve = ''
    scy_section_valve = ''


    for macro in macro_list:
        for _ in micro_list:
            scy_valve += f"valve{macro}.scy "
            scy_section_valve += f"valve{macro}.scy : valve{macro}.shy\n\n"

    scy_section_valve = scy_section_valve[:-2]

    result = template_makefile.replace('{VALVE_FILES_SCY}', scy_valve)
    result = result.replace('{VALVE_FILES_SCY_SECTION}', scy_section_valve)

    with path.open(mode="w") as f:
        f.write(result)

# def create_exclude_base_systems(path: Path, macro_list: List[int]):
#     exclude_list = list()
#
#     for macro in macro_list:
#         exclude_list.append(f"valve{macro}.shy")
#
#     with path.open(mode="w") as f:
#         yaml.dump(exclude_list, f)

if __name__ == '__main__':
    args: argparse.Namespace = create_parser().parse_args()

    if args.verbosity:
        logger.setLevel(logging.DEBUG)

    uses: Dict[str, str] = {}

    macro_list: List[int] = args.macro
    if len(macro_list) == 1:
        macro_list = [x for x in range(1, macro_list[0] + args.macro_step, args.macro_step)]

    micro_list: List[int] = args.micro
    if len(micro_list) == 1:
        micro_list = [x for x in range(1, micro_list[0] + args.micro_step, args.micro_step)]

    for macro in macro_list:
        uses[f"Valve{macro}"] = f"valve{macro}.scy"
        create_valve(Path(args.output, f"valve{macro}.shy"), macro)

    dump(Path(args.output, "uses.yml"), uses)
    create_makefile(Path(args.output, "Makefile"), macro_list, micro_list)
    # create_exclude_base_systems(Path(args.output, "exclude.yml"), macro_list)
