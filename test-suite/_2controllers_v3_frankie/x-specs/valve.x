base Valve {

  initial on -> READY:off, ERROR:on;

  final off -> OK:on;

}