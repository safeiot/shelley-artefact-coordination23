Controller(a: Valve, b: Valve, t: Timer) {

    initial level1 -> OK:level1, OK:level2, ERROR:level1 {
        match a.on {
            case Valve.READY:
                t.wait;
                a.off;
                OK;
            case Valve.ERROR:
                ERROR;
        }}

   final level2 -> ERROR:level1, OK:level1 {
        match b.on {
            case Valve.READY:
                t.wait;
                b.off;
                OK;
            case Valve.ERROR:
                ERROR;
        }
   }
}