from system import op_type
import asyncio

TIMER_OK = 1


class Timer:
    @op_type(TIMER_OK)
    async def wait(self, time_sec):
        await asyncio.sleep(time_sec)
        return TIMER_OK
