from system import match, subsystems, op_type, op
from timer import Timer
from valve import Valve, VALVE_READY, VALVE_ERROR

CONTROLLER_OK = 1
CONTROLLER_ERROR = 2


class Controller:
    def __init__(self):
        self.a = Valve(12)
        self.b = Valve(13)
        self.t = Timer()

        subsystems(self.a, self.b, self.t)

    @op_type(CONTROLLER_OK, CONTROLLER_ERROR)
    async def level1(self):
        res = await match(self.a.on())

        if res == VALVE_READY:
            await op(self.t.wait(30))
            await op(self.a.off())
            return CONTROLLER_OK
        elif res == VALVE_ERROR:
            return CONTROLLER_ERROR

    @op_type(CONTROLLER_OK, CONTROLLER_ERROR)
    async def level2(self):
        res = await match(self.b.on())

        if res == VALVE_READY:
            await op(self.t.wait(30))
            await op(self.b.off())
            return CONTROLLER_OK
        elif res == VALVE_ERROR:
            return CONTROLLER_ERROR
