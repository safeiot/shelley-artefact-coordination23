from system import HWValve, op_type

VALVE_OK = 1
VALVE_READY = 2
VALVE_ERROR = 3


class Valve:
    def __init__(self, pin):
        self.hw_valve = HWValve(pin)

    @op_type(VALVE_READY, VALVE_ERROR)
    async def on(self):
        if self.hw_valve.on():
            return VALVE_READY
        else:
            return VALVE_ERROR

    @op_type(VALVE_OK)
    async def off(self):
        self.hw_valve.off()
        return VALVE_OK
