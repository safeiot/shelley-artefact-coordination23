from machine import Pin
import uasyncio as asyncio
from uasyncio import Event
import time


def op_type(*args, **kwargs):
    def wrapper():
        pass

    return wrapper


def operation(*args, **kwargs):
    def wrapper():
        pass

    return wrapper


def matches(mytuple):
    def wrapper():
        pass

    return wrapper


class System:
    def __init__(self, setts):
        self.setts = setts


async def subsystems(*args, **kwargs):
    pass


async def op(fn):
    ret, data = await fn
    return ret, data


async def match(fn):
    ret, data = await fn
    return ret, data


async def first(fn_list):
    async def _run_tasks():
        tasks = [asyncio.create_task(f) for f in fn_list]

        while True:
            await asyncio.sleep(0)
            idx = 0
            for t in tasks:
                if t.done():
                    for t in tasks:
                        t.cancel()
                    return idx
                idx += 1

    first_task_idx = await _run_tasks()
    return first_task_idx


async def gather(cb1, cb2):
    await asyncio.gather(cb1, cb2)


class HWValve:
    def __init__(self, pin_number):
        self._pin = Pin(pin_number, Pin.OUT, value=1)

    def on(self):
        pass

    def off(self):
        pass


class HWLed:
    def __init__(self, pin_number):
        self._led = Pin(pin_number, Pin.OUT, value=1)

    async def on(self):
        self._led.off()  # ESP32 is flipped

    async def off(self):
        self._led.on()  # ESP32 is flipped


# https://wdi.centralesupelec.fr/boulanger/MicroPython/ESP32BE3
class HWButton:
    def __init__(self, port):
        self.flag = asyncio.ThreadSafeFlag()
        self.pin = Pin(port, Pin.IN, Pin.PULL_UP)
        self.pin.irq(trigger=Pin.IRQ_FALLING, handler=self._irq_callback)
        self.last_on = 0

    async def click(self):
        await self.flag.wait()

    def _irq_callback(self, irq):
        # Get the current date in milliseconds
        time_ms = time.ticks_ms()
        # Compute the elapsed time since the last interrupt
        delay = time.ticks_diff(time_ms, self.last_on)
        # If the delay is shorter than 200ms (your mileage may vary), consider this as a bounce
        if delay < 300:
            return
        # Else, we update the time of the last interrupt
        self.last_on = time_ms
        self.flag.set()


class HWPollingButton:
    def __init__(self, port):
        self.flag = False
        self.pin = Pin(port, Pin.IN, Pin.PULL_UP)
        self.pin.irq(trigger=Pin.IRQ_FALLING, handler=self._irq_callback)
        self.last_on = 0

    async def click(self):
        self.flag = False
        while not self.flag:
            await asyncio.sleep(0)

    def _irq_callback(self, irq):
        # Get the current date in milliseconds
        time_ms = time.ticks_ms()
        # Compute the elapsed time since the last interrupt
        delay = time.ticks_diff(time_ms, self.last_on)
        # If the delay is shorter than 200ms (your mileage may vary), consider this as a bounce
        if delay < 200:
            return
        # Else, we update the time of the last interrupt
        self.last_on = time_ms
        self.flag = True
