import uasyncio as asyncio
import sys
import machine
from controller import Controller

from system import match, subsystems


class Main:
    OK = 1

    def __init__(self):
        self.c1 = Controller()

        subsystems(self.c1)

    async def run(self):

        status1 = await match(self.c1.level1())  # blocks until timer expires
        if status1 == Controller.OK:

            # NOT DOING A MATCH ON self.c1.level2 IS AN ERROR
            status2 = await match(self.c1.level2())  # blocks until timer expires

            if status2 == Controller.OK:  # OMITTING THIS BRANCH IS AN ERROR!
                return self.OK
            elif status2 == Controller.ERROR:
                return self.OK

        elif status1 == Controller.ERROR:  # OMITTING THIS BRANCH IS AN ERROR!
            return self.OK

        # for i in range(3):
        #
        #     status1 = await match(self.c1.level1())  # waits for timer to expire
        #
        #     if status1 == Controller.OK:
        #
        #         for i in range(5):
        #
        #             status2 = await match(self.c1.level2())  # waits for timer to expire
        #
        #             if status2 == Controller.OK:
        #                 return self.OK
        #
        #             elif status2 == Controller.ERROR:
        #                 continue
        #
        #         else:
        #             print("Level2 failed after 5 times!")
        #             return self.OK
        #
        #     elif status1 == Controller.ERROR:
        #         continue
        #
        # else:
        #     print("Level1 failed after 3 times!")
        #     return self.OK


async def boot():
    m = Main()
    while True:
        await m.run()


try:
    print("Wake up!")
    asyncio.run(boot)
    machine.deepsleep(10000)
except KeyboardInterrupt as err:
    sys.print_exception(err)
except Exception as err:
    sys.print_exception(err)
    machine.reset()
