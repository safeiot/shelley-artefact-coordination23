class Controller(
	a: Valve,
    b: Valve,
    t: Timer) {

    async initial final level1 -> OK:level1, OK:level2 {
        await a.on;
		await t.wait;

		yield OK
    }

    async final standby1 -> OK:level1 {
		await a.off;

		yield OK
    }

    async level2 -> OK:standby2 {
        await b.on;
        await t.wait;
        await b.off;

        yield OK
    }

    async final level2 -> OK:level1 {
        await self.b.on;
        await self.b.off;

        yield OK
    }

}
