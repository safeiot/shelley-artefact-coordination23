TwoControllers(
	c1: Controller,
    c2: Controller) {

    initial final level1 -> ERROR:level1, OK:level2 {
        await match c1.level1 {
            case Controller.OK:
                await match c2.level1 {
		            case Controller.OK:
		                return OK;
		            case Controller.ERROR:
		                return ERROR;
                }
            case Controller.ERROR:
                return ERROR;
        }
    }

    final level2 -> ERROR:level1, ERROR:level2, OK:level1 {
        await match c1.level2 {
            case Controller.OK:
                await match c2.level2 {
		            case Controller.OK:
		                return OK;
		            case Controller.ERROR:
		                return ERROR;
                }
            case Controller.ERROR:
                return ERROR;
        }
    }

}
