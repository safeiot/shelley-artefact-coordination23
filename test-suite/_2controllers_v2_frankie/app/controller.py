import uasyncio as asyncio

from system import match, subsystems, operation, matches, next

from timer import Timer
from valve import Valve


class Controller:
    OK = 1

    def __init__(self):
        self.a = Valve()
        self.b = Valve()
        self.t = Timer()

        subsystems(self.a, self.b, self.t)

    @operation(initial=True, final=False)
    @next(
        [(OK, ("level2")), (OK, ("standby1")),]
    )
    async def level1(self):
        await self.a.on()
        await self.t.wait()
        return self.OK

    @operation(initial=False, final=True)
    @next(
        [(OK, ("level1")),]
    )
    async def standby1(self):
        await self.a.off()
        return self.OK

    @operation(initial=False, final=False)
    @next(
        [(OK, ("standby2")),]
    )
    async def level2(self):
        await self.b.on()
        await self.t.wait()
        return self.OK

    @operation(initial=False, final=True)
    @next(
        [(OK, ("level1")),]
    )
    async def standby2(self):
        await self.a.off()
        await self.b.off()
        return self.OK
