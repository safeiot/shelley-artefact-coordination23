import uasyncio as asyncio

from system import operation, next


class Timer:
    OK = 1

    @operation(initial=True, final=True)
    @next([(OK, ("wait"))])
    async def wait(self, time_sec):
        await asyncio.sleep(time_sec)
        return self.OK, None
