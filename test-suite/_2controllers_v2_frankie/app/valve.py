import uasyncio as asyncio

from system import operation, next


class Valve:
    OK = 1

    @operation(initial=True, final=True)
    @next([(OK, ("off"))])
    async def on(self):
        return self.OK

    @operation(initial=False, final=True)
    @next([(OK, ("on"))])
    async def off(self):
        return self.OK
