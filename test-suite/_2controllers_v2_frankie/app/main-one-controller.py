import uasyncio as asyncio
import sys
import machine
from controller import Controller

from system import match, subsystems


class Main:
    OK = 1

    def __init__(self):
        self.c1 = Controller()

        subsystems(self.c1)

    async def run_level1(self):
        await self.c1.level1()
        await self.c1.standby1()

    async def run_level2(self):
        await self.c1.level1()
        await self.c1.level2()
        await self.c1.standby2()


async def boot():
    m = Main()
    while True:
        await m.run_level1()
        await m.run_level2()


try:
    print("Wake up!")
    asyncio.run(boot)
    machine.deepsleep(10000)
except KeyboardInterrupt as err:
    sys.print_exception(err)
except Exception as err:
    sys.print_exception(err)
    machine.reset()
